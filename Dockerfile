# Base image with Nginx web server
FROM nginx

COPY default.conf.template /etc/nginx/conf.d/default.conf.template

# Copy your HTML files from the current directory
COPY . /usr/share/nginx/html


# Start Nginx server
CMD /bin/bash -c "envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'